﻿module SuaveMusicStore.View

open System.Net
open FSharp.Data

type Simple = JsonProvider<""" {"token_type":"bearer","access_token":"Eg...==","expires_in":86400} """>

let refreshed22 f=
    let mutable token = ""
    let mutable time = System.DateTime.Now.AddHours(-30.0)
    if (System.DateTime.Now-time).TotalHours > 22.0 then
        token <- f
        time <- System.DateTime.Now
        token
        else    token


let DeveloperAccessToken  =
    //Set url
    let url = "https://login.live.com/accesstoken.srf"

    //Configure request
    let req = HttpWebRequest.Create(url) :?> HttpWebRequest
    req.ProtocolVersion <- HttpVersion.Version11
    req.Method <- "POST"

    //Encode body with POST data
    let application_id = @"fa64ba88-6944-4a26-8eed-7e81a336745e"
    let application_secret = @"WEfdYaJwkxG5yLQcA4c9uKu"
    let grant_type = "client_credentials"
    let scope = "app.music.xboxlive.com"
    let body = sprintf "grant_type=%s&client_id=%s&client_secret=%s&scope=%s" grant_type application_id application_secret scope

    let postBytes = System.Text.Encoding.ASCII.GetBytes(body)
    req.ContentType <- "application/x-www-form-urlencoded"
    req.ContentLength <- int64 postBytes.Length

    let reqStream = req.GetRequestStream()
    reqStream.Write(postBytes, 0,postBytes.Length)
    reqStream.Close()

    //Obtain reqponse
    let resp = req.GetResponse()
    let stream = resp.GetResponseStream()
    let reader = new System.IO.StreamReader(stream)
    let json = reader.ReadToEnd()
    
    //JSON
    let simple = Simple.Parse(json)
    sprintf "Bearer %s" simple.AccessToken

