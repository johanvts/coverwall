﻿module SuaveMusicStore.App

open Suave
open Suave.Successful
open Suave.Operators
open Suave.Filters
open Suave.RequestErrors
open System.Threading
open System
open System.Net
open System.IO
open Suave.Files

let mutable name = "A"

let browse =
    request (fun r ->
             match r.queryParam "genre" with
             |Choice1Of2 genre -> OK (sprintf "Genre: %s" genre)
             |Choice2Of2 msg ->  BAD_REQUEST msg)
      

let settoken a = 
    printfn "token was %s" a

let webPart =
    choose [
        POST >=> request (fun req -> (settoken (Text.Encoding.UTF8.GetString(req.rawForm)));OK "")
        GET >=> choose [
            path "/" >=> Files.file "index.html"
            Files.browseHome
            path "/name" >=> (OK name)
            path "/store/details" >=> (OK "Details")
            path "/path" >=> (OK (Path.GetFullPath "./"))
            path "/new_file/" >=> Files.file "index.html"
            pathScan "%s" (fun mystring -> OK (sprintf "%s" mystring))
            pathScan "/store/details/%d" (fun id -> OK (sprintf "Details: %d" id))
            pathScan "/try_thisstoken=%s&%s" (fun (token,_) -> OK (sprintf "Access Token:%s" token))                  
            path "/store/browse" >=> browse
            path "/get_some/" >=> (OK (SuaveMusicStore.View.refreshed22 SuaveMusicStore.View.DeveloperAccessToken))
            ]
        RequestErrors.NOT_FOUND "I COULD NOT FIND THE FILEEE!!"           
        ]


let cts = new CancellationTokenSource()
let conf = {defaultConfig with 
                    cancellationToken = cts.Token 
                    homeFolder = Some(Path.GetFullPath "./public_html")}
let listening, server = startWebServerAsync conf webPart
Async.Start(server,cts.Token)
printfn "Make request now"
Console.ReadKey true |> ignore
Console.ReadKey true |> ignore
cts.Cancel()
