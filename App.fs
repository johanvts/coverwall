﻿open Suave
open Suave.Successful
open Suave.Operators
open Suave.Filters
open Suave.RequestErrors
open System.Net.Http
open System.Threading
open System

let browse =
    request (fun r ->
             match r.queryParam "genre" with
             |Choice1Of2 genre -> OK (sprintf "Genre: %s" genre)
             |Choice2Of2 msg ->  BAD_REQUEST msg)


let webPart =
    choose [
        path "/" >=> (OK "Home")
        path "/store" >=> (OK "Store")
        path "/store/details" >=> (OK "Details")
        pathScan "/store/details/%d" (fun id -> OK (sprintf "Details: %d" id))
        path "/store/browse" >=> browse
        path "/get_some/" >=> (OK "GOOGLE")
        ]




let cts = new CancellationTokenSource()
let conf = {defaultConfig with cancellationToken = cts.Token}
let listening, server = startWebServerAsync conf webPart
Async.Start(server,cts.Token)
printfn "Make request now"
Console.ReadKey true |> ignore
cts.Cancel()
